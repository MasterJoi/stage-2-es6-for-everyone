import { controls } from '../../constants/controls';

const {
  PlayerOneAttack,
  PlayerOneBlock,
  PlayerTwoAttack,
  PlayerTwoBlock,
  PlayerOneCriticalHitCombination,
  PlayerTwoCriticalHitCombination,
} = controls;

export async function fight(firstFighter, secondFighter) {
  const keyPressed = {}
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    const leftFighterIndicator = document.getElementById('left-fighter-indicator');
    const rightFighterIndicator = document.getElementById('right-fighter-indicator'); rightFighterIndicator

    leftFighterIndicator.style.width = '100%';
    rightFighterIndicator.style.width = '100%';

    let isBlockLeft = false;
    let isBlockRight = false;

    const state = {
      fighterLeft: { health: firstFighter.health },
      fighterRight: { health: secondFighter.health },
    };

    const time = {
      fighterLeftcooldown: { cooldown: new Date().getTime() },
      fighterRightcooldown: { cooldown: new Date().getTime() },
    };

    document.addEventListener('keydown', function (event) {
      const currentAction = event.code;
      keyPressed[currentAction] = true;

      switch (currentAction) {
        case PlayerOneAttack:
          if (!isBlockRight && !isBlockLeft) {
            state.fighterRight.health -= getDamage(firstFighter, secondFighter);
            state.fighterRight.health = state.fighterRight.health < 0 ? 0 : state.fighterRight.health;
            rightFighterIndicator.style.width = (100 * state.fighterRight.health) / secondFighter.health + '%';
            renderGettingDamage('right')
          }
          break;
        case PlayerOneBlock:
          isBlockLeft = true;
          break;
        case PlayerTwoAttack:
          if (!isBlockLeft && !isBlockRight) {
            state.fighterLeft.health -= getDamage(secondFighter, firstFighter);
            state.fighterLeft.health = state.fighterLeft.health < 0 ? 0 : state.fighterLeft.health;
            leftFighterIndicator.style.width = (100 * state.fighterLeft.health) / firstFighter.health + '%';
            renderGettingDamage('left')
          }
          break;
        case PlayerTwoBlock:
          isBlockRight = true;
          break;

        default:
          break;
        }
      console.log(keyPressed)
          let crit1 = firstPlayerCriticalKeyArray(time, keyPressed)
      console.log(crit1)
          if (crit1) {
            state.fighterRight.health -= secondFighter.attack * 2;
          state.fighterRight.health = state.fighterRight.health < 0 ? 0 : state.fighterRight.health;
          rightFighterIndicator.style.width = (100 * state.fighterRight.health) / secondFighter.health + '%';
            renderGettingDamage('right');
          }
          let crit2 = secondPlayerCriticalKeyArray(time, keyPressed)
      console.log(crit2)
          if (crit2) {
            state.fighterLeft.health -= firstFighter.attack * 2;
          state.fighterLeft.health = state.fighterLeft.health < 0 ? 0 : state.fighterLeft.health;
          leftFighterIndicator.style.width = (100 * state.fighterLeft.health) / firstFighter.health + '%';
            renderGettingDamage('left');
          }

      if (state.fighterLeft.health <= 0) {
        let fighterImg = document.querySelector(`#${firstFighter.name}`);
        fighterImg.parentNode.removeChild(fighterImg);
        resolve(secondFighter);
      } else if (state.fighterRight.health <= 0) {
        let fighterImg = document.querySelector(`#${secondFighter.name}`);
        fighterImg.parentNode.removeChild(fighterImg);
        resolve(firstFighter);
      }
    });

    document.addEventListener('keyup', function (event) {
      const currentAction = event.code;
      delete keyPressed[currentAction];
      switch (currentAction) {
        case controls.PlayerOneBlock:
          isBlockLeft = false;
          break;

        case controls.PlayerTwoBlock:
          isBlockRight = false;
          break;

        default:
          undoGettingDamage('left');
          undoGettingDamage('right');
          break;
      }
    });
  });
}

export function getDamage(attacker, defender) {
  // return damage

  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  return blockPower > hitPower ? 0 : hitPower - blockPower;
}

export function getHitPower(fighter) {
  // return hit power
  const { attack } = fighter;
  return attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  // return block power
  const { defense } = fighter;
  return defense * (Math.random() + 1);
}

function firstPlayerCriticalKeyArray(time, keyPressed) {
  if (PlayerOneCriticalHitCombination[0] in keyPressed && PlayerOneCriticalHitCombination[1] in keyPressed
    && PlayerOneCriticalHitCombination[2] in keyPressed && time.fighterLeftcooldown.cooldown < new Date().getTime()) {
    time.fighterLeftcooldown.cooldown = new Date().getTime() + 10000;
    return true;
  } else return false;
}

function secondPlayerCriticalKeyArray(time, keyPressed) {
  if (PlayerTwoCriticalHitCombination[0] in keyPressed && PlayerTwoCriticalHitCombination[1] in keyPressed
    && PlayerTwoCriticalHitCombination[2] in keyPressed && time.fighterRightcooldown.cooldown < new Date().getTime()) {
    time.fighterRightcooldown.cooldown = new Date().getTime() + 10000;
    return true;
  } else return false;
}

function renderGettingDamage(position) {

  let fighterImg = document.querySelector(`#${position}-fighter-img img`);

  fighterImg.className = 'fighter-damaged-preview___img';
}

function undoGettingDamage(position) {

  let fighterImg = document.querySelector(`#${position}-fighter-img img`);

  fighterImg.className = 'fighter-preview___img';
}