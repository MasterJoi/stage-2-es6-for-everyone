import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {

  if (!fighter) {
    const textBlock = createElement({ tagName: 'div' });
    textBlock.insertAdjacentHTML('beforeend',
      `<p class='arena___fighter-name'> Select the opponent!</p>`)
    return textBlock;
  }
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  fighterElement.append(createFighterPreviewImage(fighter, position));

  const info_position = positionClassName === 'fighter-preview___right' ? 'beforeend' : 'afterbegin';
  fighterElement.insertAdjacentHTML(`${info_position}`, `
    <div class=${positionClassName}>
    <p class='arena___fighter-name'>Name: ${fighter.name}</p>
    <p class='arena___fighter-name'>Helth: ${fighter.health}</p>
    <p class='arena___fighter-name'>Defense: ${fighter.defense}</p>
    <p class='arena___fighter-name'>Attack: ${fighter.attack}</p>
    </div>
    
    `)
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    id: name,
    src: source, 
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterPreviewImage(fighter, position) {
  const imgPositionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const { source, name } = fighter;
  const attributes = {
    src: source,
    width: 200,
    height: 300
  };
  const previewImgElement = createElement({
    tagName: 'img',
    className: `fighter-preview___img ${imgPositionClassName}`,
    attributes
  });

  return previewImgElement;
}